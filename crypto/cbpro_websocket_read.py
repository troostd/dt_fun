## Coinbase Pro Websocket Read

from websocket import create_connection
import json
import time

url = r'wss://ws-feed-public.sandbox.pro.coinbase.com'

ws = create_connection(url)

params = {
        "type": "subscribe",
        "channels": [{"name": "ticker", "product_ids": ["ETH-USD"]}]
}

while True:
    ws.send(json.dumps(params))
    result = ws.recv()
    print(result)
    time.sleep(1)
    converted = json.loads(result)
