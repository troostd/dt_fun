#### Crypto Analysis - DT 2020/03/10 -
#### About: A quick correlation observation between ETH, BTC, and the US Stock Index

import pandas as pd
import pandas_datareader.data as web
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from datetime import date
from datetime import datetime
import os
import requests
import pprint


## Input Parameters
dirStem = r"C:\Users\troostd\Documents\data"
dirSubData = "crypto"
dirData = os.path.join(dirStem, dirSubData)

dateStart = date(2020, 1, 1)
#date_end = datetime.datetime(2020, 3, 1)
#dateEnd = datetime(date.today())
dateEnd = date.today()
dateDelta = (dateEnd - dateStart).days

symbolMarket = '^GSPC'
symbolCrypto = 'ETH'
symboCryptoBase = 'USD'

exchangeMarket = 'yahoo'
exchangeCrypto = 'Kraken'

pp = pprint.PrettyPrinter(indent=4)
##


def get_filename(from_symbol, to_symbol, exchange, datetime_interval, download_date):

    #return '%s_%s_%s_%s_%s.csv' % (from_symbol, to_symbol, exchange, datetime_interval, download_date)
    return '{0}_{1}_{2}_{3}_{4}.csv'.format(from_symbol,
        to_symbol,
        exchange,
        datetime_interval,
        download_date)


def read_dataset(filename):

    print('Reading data from %s' % filename)
    df = pd.read_csv(filename)
    df.datetime = pd.to_datetime(df.datetime) # change to datetime
    df = df.set_index('datetime')
    df = df.sort_index() # sort by datetime
    print(df.shape)

    return df


def download_crypto_data(from_symbol, to_symbol, exchange, datetime_interval):

    supported_intervals = {'minute', 'hour', 'day'}
    assert datetime_interval in supported_intervals,\
        'datetime_interval should be one of %s' % supported_intervals
    print('Downloading %s trading data for %s %s from %s' %
          (datetime_interval, from_symbol, to_symbol, exchange))
    base_url = 'https://min-api.cryptocompare.com/data/histo'
    url = '%s%s' % (base_url, datetime_interval)
    params = {'fsym': from_symbol,
        'tsym': to_symbol,
        'limit': dateDelta,
        'aggregate': 1,
        'e': exchange}
    request = requests.get(url, params=params)
    data = request.json()
    print(data)

    return data


def download_market_data(symbol, exchange, start, end):

    df_market = web.DataReader(symbol, exchange, start, end)
    #print(df_market)

    return df_market


def format_crypto_data(data_eth_usd):

    data = []
    lst_date = []
    lst_close = []
    for dd in data_eth_usd:
        ts = int(dd["time"])
        time = datetime.utcfromtimestamp(ts).strftime('%Y-%m-%d')
        close = dd["close"]
        lst_date.append(time)
        lst_close.append(close)
        data.append([time, close])

    df_crypto = pd.DataFrame(lst_close, index=lst_date, columns=['Close_Crypto'])
    #print(df_crypto)
    return df_crypto


def do_plots(data1, data2, data3, data4, data5):

    #plt.figure(figsize=(6, 9))
    fig, ax = plt.subplots(3,1, figsize=(9, 5))

    ax[0].plot(data1, data2)
    ax[0].xaxis.set_major_locator(mdates.MonthLocator())
    ax[0].xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m'))
    ax[0].xaxis.grid(True)
    ax[0].tick_params(axis='both', which='major', labelsize=8)
    plt.setp(ax[0].get_xticklabels(), visible=False)

    ax[1].plot(data1, data3)
    ax[1].xaxis.set_major_locator(mdates.MonthLocator())
    ax[1].xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m'))
    ax[1].xaxis.grid(True)
    ax[1].tick_params(axis='both', which='major', labelsize=8)
    plt.setp(ax[0].get_xticklabels(), visible=True)

    ax[2].plot(data1, data4)
    ax[2].plot(data1, data5)
    ax[2].xaxis.set_major_locator(mdates.MonthLocator())
    ax[2].xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m'))
    ax[2].xaxis.grid(True)
    ax[2].tick_params(axis='both', which='major', labelsize=8)
    plt.setp(ax[0].get_xticklabels(), visible=True)

    #plt.figure(figsize=(6, 9))
    #plt.subplot(3, 1, 1)
    #plt.plot(data1, data2)
    #print(ax)
    #print(enumerate(ax.flatten()))
    plt.show()


def do_analysis(frame):
    deltaClosePctMarket = []
    deltaClosePctCrypto = []

    for ii in range(len(frame.index)):

        if ii == 0:
            deltaPctMarket = np.nan
            deltaPctCrypto = np.nan
        else:
            deltaPctMarket = round((frame['Close'][ii] - frame['Close'][ii-1])*100/frame['Close'][ii-1], 2)
            deltaPctCrypto = round((frame['Close_Crypto'][ii] - frame['Close_Crypto'][ii-1])*100/frame['Close_Crypto'][ii-1], 2)

        deltaClosePctMarket.append(deltaPctMarket)
        deltaClosePctCrypto.append(deltaPctCrypto)

    df_merged['deltaClosePctMarket'] = deltaClosePctMarket
    df_merged['deltaClosePctCrypto'] = deltaClosePctCrypto

    print(df_merged)
    print(df_merged.corr(method='pearson'))


if __name__ == "__main__":

    get_filename('ETH', 'USD', 'Kraken', 'day', dateEnd)
    #pp.pprint(download_crypto_data('ETH', 'USD', 'Bitstamp', 'day')['Data'])
    #data_eth_usd = download_crypto_data('ETH', 'USD', 'Kraken', 'day')["Data"]
    #df_crypto = format_crypto_data(data_eth_usd)
    df_market = download_market_data(symbolMarket, exchangeMarket, dateStart, dateEnd)
    df_merged = df_market.merge(df_crypto, how='right', left_index=True, right_index=True)

    do_analysis(df_merged)
    #print(data_eth_usd_f[-2])
    do_plots(df_merged.index,
        df_merged['Close'],
        df_merged['Close_Crypto'],
        df_merged['deltaClosePctMarket'],
        df_merged['deltaClosePctCrypto'] )
