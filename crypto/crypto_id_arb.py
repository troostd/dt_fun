
from kucoin.client import Client as KuCoin_Client
from binance.client import Client as Binance_Client
import time

kucoin_api_key = ''
kucoin_api_secret = ''
kucoin_api_passphrase = ''

binance_api_key = ''
binance_api_secret = ''
binance_api_passphrase = ''

class ConnectCrypto(object):

    def __init__(self, exchange, api_key, api_secret, api_passphrase):

        self.exchange = exchange
        self.api_key = api_key
        self.api_secret = api_secret
        self.api_passphrase = api_passphrase

        if self.exchange == 'kucoin':
            self.client = KuCoin_Client(self.api_key, self.api_secret, self.api_passphrase)

        if self.exchange == 'binance':
            self.client = Binance_Client(self.api_key, self.api_secret, self.api_passphrase)


    def search_currencies(self, symbol):

        currencies = self.client.get_currencies()
        for c in currencies:
            if c['name'] == symbol:
                print(c)
                break
            return c['name']


    def get_orderbook(self, pair):

        depth = self.client.get_order_book(pair)
        return depth




    def get_price(self, pair):

        if self.exchange == 'kucoin':
            ticker = self.client.get_ticker(pair)
            return ticker, rnd(ticker['bestAsk']), ticker['bestAskSize'],  rnd(ticker['bestBid']), ticker['bestBidSize'], ticker['price']

        if self.exchange == 'binance':
            ticker = self.client.get_ticker()
            for t in ticker:
                if t['symbol'] == pair:
                    break
            return t,  rnd(t['askPrice']), t['askQty'],  rnd(t['bidPrice']), t['bidQty'], t['lastPrice']

        #return ticker, ticker['bestAsk'], ticker['bestAskSize'], ticker['bestBid'], ticker['bestBidSize'], ticker['price']

def rnd(input):

    num = round(float(input), 6)
    return num


KuCoin = ConnectCrypto('kucoin', kucoin_api_key, kucoin_api_secret, kucoin_api_passphrase)
k_ticker, k_ask, k_askSize, k_bid, k_bidSize, k_price = KuCoin.get_price('ETH-BTC')
k_str = "KuCoin - Ask: {}  |  Bid: {}".format(k_ask, k_bid)
print(k_str)

Binance = ConnectCrypto('binance', binance_api_key, binance_api_secret, binance_api_passphrase)
b_ticker, b_ask, b_askSize, b_bid, b_bidSize, b_price = Binance.get_price('ETHBTC')
b_str = "Binance - Ask: {}  |  Bid: {}".format(b_ask, b_bid)
print(b_str)
print("")
print((k_bid - b_ask ), b_bid - k_ask)


'''
for ii in range(1000):

    k_ticker, k_ask, k_askSize, k_bid, k_bidSize, k_price = KuCoin.get_price('ETH-BTC')
    b_ticker, b_ask, b_askSize, b_bid, b_bidSize, b_price = Binance.get_price('ETHBTC')

    k_delta = k_bid - b_ask
    b_delta = b_bid - k_ask

    if (k_delta > 0) | (b_delta > 0):
        str = "Arb! - k_delta: {}  |  b_delta: {}".format(k_delta, b_delta)
        print(str)

    time.sleep(1)
'''

def process_message(msg):
    print("message type: {}".format(msg['e']))
    print(msg)
    # do something


from binance.websockets import BinanceSocketManager
bm = BinanceSocketManager(Binance)
# start any sockets here, i.e a trade socket
conn_key = bm.start_miniticker_socket(process_message, 1000)
# then start the socket manager
bm.start()




#symbol = KuCoin.search_currencies('ETH')
#orders = KuCoin.get_orderbook('ETH-BTC')

#ticker, bestAsk, bestAskSize, bestBid, bestBidSize, price = KuCoin.get_price('ETH-BTC')
