## 5/18/2018 - Dan Troost
## https://pimylifeup.com/raspberry-pi-temperature-sensor/

import os
import glob
import time
from RPi import GPIO

GPIO.setmode(GPIO.BCM)

## temperature sensor

try:

    temp_low = 27
    ##temp_low = 34
    
    temp_high = 30
    ##temp_high = 36.8
    
    temp_lag = .3

    os.system('modprobe w1-gpio')
    os.system('modprobe w1-therm')
     
    base_dir = '/sys/bus/w1/devices/'
    device_folder = glob.glob(base_dir + '28*')[0]
    device_file = device_folder + '/w1_slave'

    def read_temp_raw():
        f = open(device_file, 'r')
        lines = f.readlines()
        f.close()
        return lines

    def read_temp():
        lines = read_temp_raw()
        while lines[0].strip()[-3:] != 'YES':
            time.sleep(0.2)
            lines = read_temp_raw()
        equals_pos = lines[1].find('t=')
        if equals_pos != -1:
            temp_string = lines[1][equals_pos+2:]
            temp_c = float(temp_string) / 1000.0
            temp_f = temp_c * 9.0 / 5.0 + 32.0
            return temp_c, temp_f
        
    def enable(enable_pin):
        GPIO.setup(enable_pin, GPIO.OUT, initial=GPIO.HIGH)
        
    def disable(disable_pin):
        GPIO.setup(disable_pin, GPIO.OUT, initial=GPIO.LOW)    

    #enable(6)
    disable(6)

    while True:
        temp_fridge = read_temp()[1]
        #print(temp_fridge)	
        time.sleep(0.5)
            
        if temp_fridge > temp_high:
            enable(6)  #enable fridge cooling
            enable(13)  #enable USB fan
            #print("")
            #print("enabled!")
            time.sleep(5)

        if temp_fridge < (temp_high + temp_low + temp_lag)/2:
            disable(6)  #disable fridge cooling
            disable(13)  #disable fridge fan
            time.sleep(3)

    #GPIO.output(13,1)

except KeyboardInterrupt:
    print("Error: Keyboard Interrupt")
    GPIO.cleanup()

except:
    print("Error: Something!")

finally:
    GPIO.cleanup()
