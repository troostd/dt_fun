from laspy.file import File
import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import dt_csv
import math

#wd = r"C:\Users\troostd\Documents\amznrepo\data"
#inFile = File((wd+'points.las'), mode='r')
##inFile = File(('points.las'), mode='r')

dirStem = r"C:\Users\troostd\Documents\data"
dirSubData = "drone"
#dataFile = "points.las"
dataFile = "oli1_group1_densified_point_cloud_part_1.las"
dirData = os.path.join(dirStem, dirSubData)
inFile = File(os.path.join(dirData, dataFile), mode='r')

##print(inFile.point_format)
#print(inFile) 567667

# where to cut the X, Y image from 0-100 left-right, up-down
CUT_PCT = .70
CUT_Z_ELEV = -8.5

d_drone = pd.DataFrame({
    'point' : inFile.points,
    'x' : inFile.x,
    'y' : inFile.y,
    'z' : inFile.z,
    'red' : inFile.red,
    'green' : inFile.green,
    'blue' : inFile.blue,
    ##'intensity' : inFile.intensity
})

##print(d_drone[0, 'point'])

msg = "Drone Observations: {} -- min: {} max: {} range: {}".format("x", min(d_drone['x']), max(d_drone['x']), (max(d_drone['x']) - min(d_drone['x'])))
print(msg)

msg = "Drone Observations: {} -- min: {} max: {} range: {}".format("y", min(d_drone['y']), max(d_drone['y']), (max(d_drone['y']) - min(d_drone['y'])))
print(msg)

msg = "Drone Observations: {} -- min: {} max: {} range: {}".format("z", min(d_drone['z']), max(d_drone['z']), (max(d_drone['z']) - min(d_drone['z'])))
print(msg)

# normalize x,y values around (0,0)  -- PRE-CUT
d_drone['x_norm'] = d_drone['x'] - ((max(d_drone['x']) + min(d_drone['x']))/2)
d_drone.drop('x', axis=1, inplace=True)
d_drone.rename(columns={'x_norm':'x'}, inplace=True)

d_drone['y_norm'] = d_drone['y'] - ((max(d_drone['y']) + min(d_drone['y']))/2)
d_drone.drop('y', axis=1, inplace=True)
d_drone.rename(columns={'y_norm':'y'}, inplace=True)

#print("number of rows: ", d_drone.count())

#d_drone_plt = d_drone.head(n=3825200)
#d_drone_plt = d_drone.head(n=38433261)

d_drone_plt = d_drone.loc[::4]
d_drone_plt = d_drone_plt.loc[d_drone.z <= CUT_Z_ELEV,:]
d_drone_plt = d_drone_plt.loc[d_drone.x >= min(d_drone['x']) + ((max(d_drone['x']) - min(d_drone['x'])) * CUT_PCT),:]
#d_drone_plt = d_drone_plt.loc[d_drone.x >= 567667,:]
#d_drone_plt = d_drone_plt.loc[d_drone.y <= 6044206.565,:]
d_drone_plt = d_drone_plt.loc[d_drone.y >= min(d_drone['y']) + ((max(d_drone['y']) - min(d_drone['y'])) * CUT_PCT),:]

#plt.ylim((6044150.02, 6044206.565))
#plt.xlim((-13592000.13, -13591959.48))

#d_drone_plt = d_drone_plt.loc[::6]
print("number of rows: ", d_drone_plt.count())

msg = "Drone Observations--POST-CUT: {} -- min: {} max: {} range: {}".format("x", min(d_drone_plt['x']), max(d_drone_plt['x']), (max(d_drone_plt['x']) - min(d_drone_plt['x'])))
print(msg)

msg = "Drone Observations--POST-CUT: {} -- min: {} max: {} range: {}".format("y", min(d_drone_plt['y']), max(d_drone_plt['y']), (max(d_drone_plt['y']) - min(d_drone_plt['y'])))
print(msg)

msg = "Drone Observations--POST-CUT: {} -- min: {} max: {} range: {}".format("z", min(d_drone_plt['z']), max(d_drone_plt['z']), (max(d_drone_plt['z']) - min(d_drone_plt['z'])))
print(msg)


# normalize x,y values around (0,0)  -- POST-CUT
d_drone_plt['x_norm'] = d_drone_plt['x'] - ((max(d_drone_plt['x']) + min(d_drone_plt['x']))/2)
d_drone_plt.drop('x', axis=1, inplace=True)
d_drone_plt.rename(columns={'x_norm':'x'}, inplace=True)


# shift to only >=0 values
d_drone_plt['x_norm'] = d_drone_plt['x'] + (abs(min(d_drone_plt['x'])))
d_drone_plt.drop('x', axis=1, inplace=True)
d_drone_plt.rename(columns={'x_norm':'x'}, inplace=True)


# normalize x,y values around (0,0)  -- POST-CUT
d_drone_plt['y_norm'] = d_drone_plt['y'] - ((max(d_drone_plt['y']) + min(d_drone_plt['y']))/2)
d_drone_plt.drop('y', axis=1, inplace=True)
d_drone_plt.rename(columns={'y_norm':'y'}, inplace=True)


# shift to only >=0 values
d_drone_plt['y_norm'] = d_drone_plt['y'] + (abs(min(d_drone_plt['y'])))
d_drone_plt.drop('y', axis=1, inplace=True)
d_drone_plt.rename(columns={'y_norm':'y'}, inplace=True)


# flip the z values for Chirality adjustment
##d_drone_plt['z_flip'] = d_drone_plt['z'] * -1
##d_drone_plt.drop('z', axis=1, inplace=True)
##d_drone_plt.rename(columns={'z_flip':'z'}, inplace=True)


print("number of rows: ", d_drone_plt.count())

msg = "Drone Observations--POST-CUT: {} -- min: {} max: {} range: {}".format("x", min(d_drone_plt['x']), max(d_drone_plt['x']), (max(d_drone_plt['x']) - min(d_drone_plt['x'])))
print(msg)

msg = "Drone Observations--POST-CUT: {} -- min: {} max: {} range: {}".format("y", min(d_drone_plt['y']), max(d_drone_plt['y']), (max(d_drone_plt['y']) - min(d_drone_plt['y'])))
print(msg)

msg = "Drone Observations--POST-CUT: {} -- min: {} max: {} range: {}".format("z", min(d_drone_plt['z']), max(d_drone_plt['z']), (max(d_drone_plt['z']) - min(d_drone_plt['z'])))
print(msg)

#d_drone_csv = d_drone.head(n=10)
d_drone_csv = d_drone_plt[['x','y','z','red','green','blue']]

#print(d_drone_csv)
#print(d_drone_plt.loc[:,'x'].max())
#print(d_drone_plt.loc[:,'x'].min())
#print(d_drone_plt.loc[:,'y'].min())
#print(d_drone_plt.loc[:,'y'].max())


d_drone_csv.to_csv(
path_or_buf=os.path.join(dirData, 'drone_terrain_test.csv'),
sep=',',
na_rep='',
columns=None,
header=True,
encoding=None,
date_format=None,
index=False,
decimal='.'
)


'''
d_drone_csv.to_csv(
    path_or_buf=(wd + 'drone_out_temp.csv'),
    mode = 'a', #append to existing
    sep=',',
    na_rep='',
    columns=None,
    header=True,
    encoding=None,
    date_format=None,
    decimal='.'
    )
'''

# summarize z values into array

#d_drone_plt = d_drone_plt[d_drone_plt['y'] <= 27.6310000000521]
d_drone_plt = d_drone_plt[d_drone_plt['y'] <= 27.6299999998882]
d_drone_plt = d_drone_plt[d_drone_plt['x'] <= 27.6299999998882]


msg = "Drone Observations--POST-CUT: {} -- min: {} max: {} range: {}".format("x", min(d_drone_plt['x']), max(d_drone_plt['x']), (max(d_drone_plt['x']) - min(d_drone_plt['x'])))
print(msg)

msg = "Drone Observations--POST-CUT: {} -- min: {} max: {} range: {}".format("y", min(d_drone_plt['y']), max(d_drone_plt['y']), (max(d_drone_plt['y']) - min(d_drone_plt['y'])))
print(msg)

avg_z_array = []
panels_x = 500 + 500
panels_y = 500 + 500
panel_factor_x = 0.027629999999888200000
panel_factor_y = 0.027629999999888200000


for y in reversed(range(1, panels_y + 1)):
    d_drone_panel = d_drone_plt.loc[(d_drone_plt['y'] > ((y-1)*panel_factor_y)) & (d_drone_plt['y'] <= ((y)*panel_factor_y))]

    print(y)

    for x in range(panels_x):
         cnt_obs = d_drone_panel.loc[(d_drone_panel['x'] > (x*panel_factor_x)) & (d_drone_panel['x'] <= ((x+1)*panel_factor_x)), 'z'].shape[0]
         sum_obs = d_drone_panel.loc[(d_drone_panel['x'] > (x*panel_factor_x)) & (d_drone_panel['x'] <= ((x+1)*panel_factor_x)), 'z'].sum()
         avg_z = round(sum_obs/cnt_obs, 4)

         if math.isnan(avg_z):
             avg_z = -10
             #print("NaN to:", avg_z)

         avg_z_array.append(avg_z)

print("length of z list:", len(avg_z_array))

dt_csv.list_to_csv_out(avg_z_array, dirData, 'avg_z_array.csv', mode='w')


plt.clf()

colors = []

for index, row in d_drone_plt.iterrows():
    tup = (round(row['red']/65500, 3), round(row['green']/65500, 3), round(row['blue']/65500, 3))
    colors.append(tup)

#print(colors)

##[(d_drone_plt['red'][p], d_drone_plt['green'], d_drone_plt['blue']) for ]
fig = plt.figure(figsize=(5, 5), dpi=160)
ax = plt.Axes(fig, [0., 0., 1., 1.])
ax.set_axis_off()
fig.add_axes(ax)

plt.scatter(
    x = d_drone_plt['x'],
    y = d_drone_plt['y'],
    alpha = 1,
    c = colors,
    ##cmap = "Greens",
    s = 0.08
    )

#plt.ylim((6044150.02, 6044206.565))
#plt.xlim((-13592000.13, -13591959.48))
##plt.tight_layout()
plt.axis('off')

#plt.figure(figsize=(5,5))
#plt.axes(frameon=False)

##plt.show()
...

plt.savefig('drone_11_08.png', bbox_inches=0, dpi=160, transparent=True, pad_inches=0.0)
#print(d_drone_plt.z.max())
