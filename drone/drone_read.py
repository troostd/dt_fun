
## 1. Read a drone LAS or CSV file for interrogating X,Y,Z and R,G,B values
## 2. Define a function to efficiently return the elevation Z based on some approximate X and Y inputs
## 2020-06 || D. Troost

import csv
import pandas as pd
import time

# read the drone file csv into a pandas data frame
df_drone = pd.read_csv(r"C:\Users\troostd\Documents\data\drone\droneData_500k_color.csv", usecols=['x','y','z'])
lookup_x = df_drone.iloc[(df_drone['y']-6044175).abs().argsort()[:300]]
lookup_y = lookup_x.iloc[(lookup_x['x']-5.1).abs().argsort()[:1]]

# cut down drone frame before lookup up?
print(df_drone)



# function to return elevation Z based on X and Y input approximation
def find_drone_elevation(frame, lookup_x, lookup_y):

    filter_y = frame.iloc[(frame['y']-lookup_y).abs().argsort()[:50]]
    filter_x = filter_y.iloc[(filter_y['x']-lookup_x).abs().argsort()[:1]]

    #print(filter_x['z'])
    return(filter_x['z'])

timeStart = time.time()
loops = range(50)

for ii in loops:
    drone_z = find_drone_elevation(df_drone, -5.1, -6044175)

timeEnd = time.time()

performance = "\nCONSOLE:\neach of {} elevation lookups took {} seconds \n".format(len(loops), round((timeEnd-timeStart)/len(loops), 2))
print(performance)
