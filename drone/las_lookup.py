from laspy.file import File
import pandas as pd

mm_to_feet = 3.28/1000

class DroneZLookup(object):

    def __init__(self, filename, x, y, lookup_radius_mm):

        self.droneFilename = filename
        self.inFile = File((self.droneFilename), mode='r')
        self.sprinklerX = x
        self.sprinklerY = y
        self.lookupRadius= lookup_radius_mm

        self.dataDrone = pd.DataFrame({
            #'point' : inFile.pt_src_id,
            #'point' : inFile.gps_time,
            'x' : self.inFile.X,
            'y' : self.inFile.Y,
            'z' : self.inFile.Z,
            'red' : self.inFile.red,
            'green' : self.inFile.green,
            'blue' : self.inFile.blue
            #'intensity' : inFile.intensity
        })

    #find elevation (Z) of sprinkler
    def get_sprinkler_z(self):
        self.dataDroneFocus = self.dataDrone.loc[(self.dataDrone.x <= self.sprinklerX + self.lookupRadius) & (self.dataDrone.x >= self.sprinklerX - self.lookupRadius),:]
        self.dataDroneFocus = self.dataDroneFocus.loc[(self.dataDroneFocus.y <= self.sprinklerY + self.lookupRadius) & (self.dataDroneFocus.y >= self.sprinklerY - self.lookupRadius),:]
        self.sprinklerZ = sum(self.dataDroneFocus.z)/len(self.dataDroneFocus.z)

    #find elevation (Z) from point cloud data by using either relative [x, y] or [turn_degrees, distance] w.r.t. a zero-degree reference point
    def get_target_z_xy(self, target_x, target_y):
        self.dataDroneFocus = self.dataDrone.loc[(self.dataDrone.x <= target_x + self.lookupRadius) & (target_x >= self.sprinklerX - self.lookupRadius),:]
        self.dataDroneFocus = self.dataDroneFocus.loc[(self.dataDroneFocus.y <= target_y + self.lookupRadius) & (self.dataDroneFocus.y >= target_y - self.lookupRadius),:]
        self.targetZ = sum(self.dataDroneFocus.z)/len(self.dataDroneFocus.z)


## in this point cloud, the sprinkler is put in the ground at x=697650, y=664950
oliverYardCloud = DroneZLookup(r'data\test4_group1_densified_point_cloud_part_1.las', 697700, 664900, 100)
oliverYardCloud.get_sprinkler_z()
print(round(oliverYardCloud.sprinklerZ*mm_to_feet,1))

oliverYardCloud.get_target_z_xy(711200, 650000)
print(round(oliverYardCloud.targetZ*mm_to_feet,1))

oliverYardCloud.get_target_z_xy(698100, 677000)
print(round(oliverYardCloud.targetZ*mm_to_feet,1))
