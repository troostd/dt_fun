
class FloodSort():
    def __init__(self, world):
        self.world = world
        #self.starting_point = starting_point
        self.world_sorted = []
        self.x_start = 0
        self.y_start = 0

        self.find_start()

        self.flood_fill({'x' : self.x_start, 'y' : self.y_start})

    def find_start(self):
        for pt in self.world:
            x = pt['x']
            y = pt['y']

            if abs(x) > self.x_start:
                self.x_start = x
                self.y_start = y
                #print("starter!", self.x_start, self.y_start)

    def flood_fill(self, pt):
        #print(pt)

        if pt not in self.world:
            #print(self.world)
            #print("true!")
            self.world_sorted.append(pt)
            #world.remove(pt)

            return

        else:
            self.world_sorted.append(pt)
            self.world.remove(pt)

            x = pt['x']
            y = pt['y']

            #left
            pt_new_l = {
                'x' : x - 1,
                'y' : y
                }

            pt_new_r = {
                'x' : x + 1,
                'y' : y
                }

            pt_new_u = {
                'x' : x,
                'y' : y + 1
                }

            pt_new_d = {
                'x' : x,
                'y' : y - 1
                }

            if pt_new_l in self.world:
                self.flood_fill( pt_new_l )
            if pt_new_r in self.world:
                self.flood_fill( pt_new_r )
            if pt_new_u in self.world:
                self.flood_fill( pt_new_u )
            if pt_new_d in self.world:
                self.flood_fill( pt_new_d )

def main():
    world = [{'x' : 1.0, 'y' : 1.0},
        {'x' : 2.0, 'y' : 1.0},
        {'x' : 3.0, 'y' : 1.0},
        {'x' : 1.0, 'y' : 2.0},
        {'x' : 2.0, 'y' : 2.0},
        {'x' : 3.0, 'y' : 2.0},
        {'x' : 1.0, 'y' : 3.0},
        {'x' : 2.0, 'y' : 3.0},
        {'x' : 3.0, 'y' : 3.0}
        ]

    sorted = FloodSort(world)

    print("world sorted at the end: ", sorted.world_sorted)


if __name__ == '__main__':
    main()

    #print("world: ", world)
