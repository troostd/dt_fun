## extra functions for handling csv data

import csv
import os



def list_to_csv_out(data, path, filename, mode='w'):
    '''write data in list of shape [list1, *list2] to .csv'''

    path_full = os.path.join(path, filename)

    with open(path_full, mode=mode, newline='') as file_object:
        csv_writer = csv.writer(file_object, delimiter=',')

        for row in data:
            if type(row) is list:
                csv_writer.writerow(row)
            else:
                csv_writer.writerow([row])

    msg = '\nSuccessfully wrote to {} !!\n'.format(filename)
    print(msg)



def csv_to_list_in(path, filename, mode='r'):
    '''read .csv file into a list of shape [[row1], *[row2]]'''

    path_full = os.path.join(path, filename)
    list_from_data = []

    try:
        with open(path_full, mode=mode) as file_object:
            csv_reader = csv.reader(file_object)
            for row in csv_reader:
                list_from_data.append(row)

    except FileNotFoundError as e:
        msg = '\nFile {} not found !!\n'.format(e.filename)
        print(msg)

    else:
        msg = '\nSuccessfully loaded {} to {} !!\n'.format(path_full, list_from_data)
        print(msg)

        return list_from_data



if __name__ == "__main__":

    ## test cases
    test_data = [[1, 2, 3, 4], [5, 6, 7, 8]]
    #test_data = [[1, 2, 3, 4]]

    list_to_csv_out(test_data, 'data', 'test.csv')
    csv_to_list_in('data', 'test.csv')
