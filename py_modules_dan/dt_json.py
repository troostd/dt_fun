## extra functions for handling json data

import json
import os



def json_out(data, path, filename, mode='w'):
    '''Open a file, write to data .json, default overwrite'''

    path_full = os.path.join(path, filename)

    with open(path_full, mode) as file_object:
        json.dump(data, file_object)

    msg = "\nSuccessfully saved {} !\n".format(path_full)
    print(msg)



def json_in(path, filename, mode='r'):
    '''Open a json file, read and store contents'''

    path_full = os.path.join(path, filename)

    try:
        with open(path_full, mode) as file_object:
            data = json.load(file_object)

    except FileNotFoundError as e:
        msg = "\nCouldn't find {} !\n".format(e.filename)
        print(msg)

    else:
        msg = "\nSuccessfully loaded {} containing {} !\n".format(filename, data)
        print(msg)

        return data



## test cases
sample_data = [1, 2, 3, 4, 5]

json_out(sample_data, 'data', 'test.json')
print(json_out.__doc__)

json_in('data', 'test2.json')
print(json_in.__doc__)
