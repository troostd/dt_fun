## customizing some plotting steps

from matplotlib import pyplot as plt
import os



def plot_line_lists(
        x, y,
        fig_x, fig_y, dpi=160,
        path='data', filename='autosave_plot_line.png'):
    '''Use pyplot for basic line plot'''

    path_full = os.path.join(path, filename)

    plt.figure(figsize=(fig_x, fig_y), dpi=dpi)
    plt.plot(x, y)
    plt.savefig(path_full)   # must come before plt.show()

    plt.show()



def plot_scatter_lists(
        x_list, y_list,
        fig_x, fig_y,
        dpi=160, alpha=0.5, color='blue', size=20,
        path='data', filename='autosave_plot_scatter.png'):
    '''Use pyplot for basic scatter plot'''

    path_full = os.path.join(path, filename)

    plt.figure(figsize=(fig_x, fig_y), dpi=dpi)
    plt.scatter(x_list, y_list, color=color, s=size, alpha=alpha)
    plt.savefig(path_full)   # must come before plt.show()

    plt.show()



## test cases
x = [1, 2, 3, 4, 5]
y = [6, 8, 13, 2, 1]
width_x = 10
width_y = 5

plot_line_lists(x, y, width_x, width_y)
plot_scatter_lists(x, y, width_x, width_y, size=50)
