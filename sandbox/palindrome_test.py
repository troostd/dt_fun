## Shadowing an interview live code question, doing it cold!

import copy

# intake string
string = "Too hot to hoot."


# core function to compare two lists
def CompareLists(list1, list2):
    if list1 == list2:
        msg = "Palindrome! {} equals {}".format(list1, list2)
    else:
        msg = "Not Palindrome! {} does not equal {}".format(list1, list2)

    print(msg)


# empty lists to fill
stringList = []
stringListCleanLower = []


# replace
stringRep = string.replace(" ", "")
stringRepClean = stringRep.replace('.', '')
stringRepCleanLower = stringRepClean.replace('T', 't')


# length of string
stringLen = len(stringRep)
stringLenCleanLower = len(stringRepCleanLower)


# parse strings into lists or sorting, comparison, parsing
for ii in range(stringLen):
    letter = stringRep[ii]
    stringList.append(letter)

for ii in range(stringLenCleanLower):
    letter = stringRepCleanLower[ii]
    stringListCleanLower.append(letter)


# reverse lists of characters, deep copy to create new compount object
stringListOrig = copy.deepcopy(stringList)
stringList.reverse()

stringListCleanLowerOrig = copy.deepcopy(stringListCleanLower)
stringListCleanLower.reverse()


# test lists for Palindromity!
if __name__ == "__main__":
    CompareLists(stringListOrig, stringList)
    CompareLists(stringListCleanLowerOrig, stringListCleanLower)
