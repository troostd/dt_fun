from matplotlib.path import Path
import matplotlib.pyplot as plt
import numpy as np
import csv
import math


class threeJSFill(object):

    # define starting values and empty lists as needed
    def __init__(self, path_xy):

        self.Z = 3  # default value of elevation to be shown in plot
        self.DIVIDER = 1  # controls the density of the grid
        self.GRID_ANGLE_RELATIVE = 0  # WIP -- to be able to rotate the grid
        self.POINTS_XY = []
        self.POINTS_XY_FILL = []
        self.tupVerts = []
        self.x_list = []
        self.y_list = []
        self.POINTS_XY_DICT = []

        # convert input to list of tuples
        for pp in range(len(path_xy)):

            x = path_xy[pp]['x']
            y = path_xy[pp]['y']
            self.x_list.append(x)
            self.y_list.append(y)
            tup = (x, y)
            self.tupVerts.append(tup)


    # function to find the max and min values (in integers, away from zero) given points
    def find_min_max(self, list_of_numbers):
        #print(list_of_numbers)

        low = math.floor(min(list_of_numbers))
        high = math.ceil(max(list_of_numbers))

        return low, high


    # find max, min, and range attributes of the given path
    def find_path_ranges(self):

        self.X_MIN, self.X_MAX = self.find_min_max(self.x_list)
        self.Y_MIN, self.Y_MAX = self.find_min_max(self.y_list)

        self.X_RANGE = self.X_MAX - self.X_MIN
        self.Y_RANGE = self.Y_MAX - self.Y_MIN

        self.RANGE_MAX = max(self.X_RANGE, self.Y_RANGE)


    # sort X,Y coordinates by finding center of mass and re-distributing radially
    def sort_radially(self, perimeter):

        center = (sum([p[0] for p in perimeter])/(len(perimeter)+0.0001),sum([p[1] for p in perimeter])/(len(perimeter)+0.0001))
        perimeter.sort(key=lambda p: math.atan2(p[1]-center[1], p[0]-center[0]))

        return perimeter


    # create x,y mesh grid for flat canvas
    def generate_fill_grid(self):

        self.x, self.y = np.meshgrid(np.arange(self.X_MIN, (self.X_MIN + self.RANGE_MAX)), np.arange(self.Y_MIN, (self.Y_MIN + self.RANGE_MAX)))
        self.GRID_SPAN = ((self.X_MIN + self.RANGE_MAX) - self.X_MIN) * ((self.Y_MIN + self.RANGE_MAX) - self.Y_MIN)

        # create z values for elevation
        self.z = np.repeat(self.Z, self.GRID_SPAN)

        # flatten meshgrid into individual arrays
        self.x, self.y = self.x.flatten()/self.DIVIDER, self.y.flatten()/self.DIVIDER
        #print(self.y)

        # create a unique index for each point
        self.i = np.arange(self.GRID_SPAN)

        # create array of points: index, x, y, and z
        self.pointSpace = np.vstack((self.i, self.x, self.y, self.z)).T

        # extract x, y from point space
        for n in self.pointSpace:
            self.POINTS_XY.append(n[1:3])

        # make a polygon from boundary vertices
        self.polygon = Path(self.tupVerts)

        # create a boolean grid based on presence of X,Y points in polygon
        self.grid = self.polygon.contains_points(self.POINTS_XY)

        # shape final output
        for n in range(len(self.pointSpace)):
            self.POINTS_XY_FILL.append(np.append(self.pointSpace[n], self.grid[n]).tolist())

        #print(self.POINTS_XY_FILL)


    # generate fill points in {} for passing back to threejs
    def generate_fill_dict(self):
        for l in self.POINTS_XY_FILL:
            dict = { 'x' : l[1] , 'y' : l[2] }
            self.POINTS_XY_DICT.append(dict)

        print(self.POINTS_XY_DICT)


if __name__ == "__main__":

    # input as json array of {x, y} values (sample as if passed from app JS)
    points = [{'x': -7.28, 'y': 7.13}, {'x': 13.16, 'y': 8.74}, {'x': 18.52, 'y': 28.12}]

    fillObject = threeJSFill(points)
    fillObject.find_path_ranges()
    fillObject.generate_fill_grid()
    fillObject.generate_fill_dict()

    # plot to check fill
    plt.scatter([point[1] for point in fillObject.POINTS_XY_FILL], [point[2] for point in fillObject.POINTS_XY_FILL], s=[point[4] for point in fillObject.POINTS_XY_FILL], color='black')
    plt.show()


# write final results to csv
"""
with open("live_shapes.csv", "w", newline="") as csv_file:
    csv_writer = csv.writer(csv_file, delimiter=",")

    for point in POINTS_XY_FILL:
        if point[4] == 1:
            csv_writer.writerow(point[1:4])
"""
    #csv_writer.writerows(POINTS_XY_FILL)

# path vertices (waypoints)
#tupVerts=[(1, 1), (1, 15), (15, 15), (15, 1)]
