from matplotlib.path import Path
import numpy as np
import csv

# define grid dimensions
GRID_POINTS_X = 50
GRID_POINTS_Y = 50
GRID_SPAN = GRID_POINTS_X * GRID_POINTS_Y
DIVIDER = 5
GRID_ANGLE_RELATIVE = 0
POINTS_XY = []
POINTS_XY_FILL = []

# path vertices (waypoints)
tupVerts=[(1, 1), (1, 15), (15, 15), (15, 1)]
# circular sort

# create a unique index for each point
i = np.arange(GRID_SPAN)

# create x,y mesh grid for flat canvas
x, y = np.meshgrid(np.arange(GRID_POINTS_X), np.arange(GRID_POINTS_Y))

# create z values for elevation
z = np.repeat(3, GRID_SPAN)

# flatten meshgrid into individual arrays
x, y = x.flatten()/DIVIDER, y.flatten()/DIVIDER

# create array of points: index, x, y, and z
pointSpace = np.vstack((i, x, y, z)).T

# extract x, y from point space
for n in pointSpace:
    POINTS_XY.append(n[1:3])

#print(POINTS_XY)
#print(len(points))

p = Path(tupVerts) # make a polygon
#print(p)
#print(p[0])
grid = p.contains_points(POINTS_XY)
#print(grid)
#mask = grid.reshape(8,8) # now you have a mask with points inside a polygon
#print(grid)
#print(mask)

for n in range(len(pointSpace)):
    POINTS_XY_FILL.append(np.append(pointSpace[n], grid[n]).tolist())

#print(POINTS_XY_FILL)

with open("live_shapes.csv", "w", newline="") as csv_file:
    csv_writer = csv.writer(csv_file, delimiter=",")

    for point in POINTS_XY_FILL:
        if point[4] == 1:
            csv_writer.writerow(point[1:4])
    #csv_writer.writerows(POINTS_XY_FILL)
