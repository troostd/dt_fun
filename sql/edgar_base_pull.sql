select 
	--fact.fact_id,
	entity.entity_name,
	fact.element_local_name,
	fact.effective_value,
	--fact.uom,
	--fact.accession_id,
	--fact.element_id,
	fact.fiscal_year
	--context.fiscal_period,
	--context.period_start,
	--context.period_end
from
	fact fact
		left join entity as entity on fact.entity_id = entity.entity_id
		left join context as context on fact.accession_id = context.accession_id 
			and fact.context_id = context.context_id
			and fact.fiscal_year = context.fiscal_year
where
	fact.element_local_name in ('GrossProfit', 
								'ProfitLoss', 
								'NetIncomeLoss'
								'Assets',
								'AssetsCurrent',
								'Liabilities',
								'LiabilitiesCurrent',
								'LongTermDebt',
								'CostsandExpenses')
	and fact.ultimus_index = 1
	and fact.fiscal_year in ('2020','2019','2018','2017','2016','2015')
	and fact.fiscal_period = 'Y'
	and entity.entity_name like ('%Apple%')	
	and context.fiscal_period = 'Y'
	
order by
	entity.entity_name,
	fact.element_local_name,
	fact.fiscal_year desc
	