#!/usr/bin/python

# import standard python libs
import os
import csv
import json
import pprint
import pandas as pd


#import connection credentials from local keys file
def import_keys(file):

    # determine path for keys file, which must be in /keys/keys_xbrl.txt local to the script
    dirCurrent = os.path.dirname(os.path.abspath(__file__))
    pathKeys = os.path.join(dirCurrent, 'keys', file)

    # open keys file JSON
    with open(pathKeys, mode = 'r') as file:
        data = json.load(file)
        username = data['user']
        password = data['password']
        file.close()

    return username, password


# run a query against the XBRL database
def xbrl_query(conn, sql):

	# conn.cursor will return a cursor object, you can use this cursor to perform SQL queries
	cursor = conn.cursor()
	cursor.execute(sql)

	# retrieve the records from the database
	records = cursor.fetchall()
	#pprint.pprint(records)

	return records


# convert list data from xbrl results to more flexible dataframe
def dat_to_dataframe(dat, cols):

    # create an empty data frame to fill
    df = pd.DataFrame(columns=cols)

    # zip columns to each row in dat, append to dataframe
    for row in dat:
        #print(row)
        dict_dat = dict(zip(cols, row))
        df = df.append(dict_dat, ignore_index=True)

    return df


# get list of distinct entities from entity TABLE_NAME
def get_distinct_entities(XBRL, re_query_ind):

    # determine local working path
    dirCurrent = os.path.dirname(os.path.abspath(__file__))
    pathEntities = os.path.join(dirCurrent, 'data', 'list_entities.csv')

    if re_query_ind == 'Y':
        # move query to .sql file?
        sqlQuery = r"SELECT distinct entity.entity_name FROM entity AS entity ORDER BY entity.entity_name"
        dat = xbrl_query(XBRL, sqlQuery)  # each data row returned as list of tuples: [('entity1',), ('entity2',)]
        df = dat_to_dataframe(dat, ['entity.entity_name'])
        df.to_csv(pathEntities, encoding='utf-8', index=False)

    else:
        # retrive list of entities from local .csv instead of hitting XBRL with query again
        df = pd.read_csv(pathEntities)

    return df


# get list of element priorities from local .csv file
def get_element_priorities(fileName):

    dirCurrent = os.path.dirname(os.path.abspath(__file__))
    pathPriorities = os.path.join(dirCurrent, 'data', fileName)

    df = pd.read_csv(pathPriorities)

    return df
