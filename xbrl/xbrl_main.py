#!/usr/bin/python

#### Generating Piotroski F-Score from SEC XBRL database
#### Strategy quick reference: https://www.investopedia.com/terms/p/piotroski-score.asp
#### Dan Troost 2020/07


# import standard python libs
import psycopg2
import sys
import json
import os
import math
import pandas as pd
import csv


# import xbrl libs
import xbrl_data_handler as DataHandler
#import xbrl_logic as LogicHandler


# run parameters
KEYS_FILE = r'keys_xbrl.txt'
SQL_FILE = r'C:\Users\troostd\Documents\code\dt_fun\sql\edgar_base_pull.sql'
PRIORITIES_FILE = r'element_priorities.csv'
REQUERY_ENTITY_LIST_IND = 'N'
NUM_ENTITIES_TO_QUERY = 5


# connect to XBRL using own credentials
def xbrl_connect(keys):

	# load user, pass from keys file stored in same directory
	username, password = DataHandler.import_keys(keys)

	# create database connection string, and establish connection
	conn_string = "host='public.xbrl.us' dbname='edgar_db' user='{}' password='{}'".format(username, password)
	conn_msg = "Connecting to database ---> {} {}".format("host='public.xbrl.us'", "dbname='edgar_db'")
	#print (conn_msg)
	conn = psycopg2.connect(conn_string)

	return conn


# read .sql file and return text string
def sql_to_text(file):

	# open file to retrieve query, close file
	with open(file, mode = 'r') as f:
		query = f.read()
		f.close()

	# return text of query
	return query


# execute main script on run
if __name__ == "__main__":

	# establish connection to edgar db XBRL
	XBRL = xbrl_connect(KEYS_FILE)

	# get list of entities
	df_entities = DataHandler.get_distinct_entities(XBRL, re_query_ind = REQUERY_ENTITY_LIST_IND)  # flip re_query indicator to 'Y' if re-downloading from XBRL, else 'N'
	list_entities = list(df_entities['entity.entity_name'])
	num_entities = len(list_entities)

	# get frame of element priorities
	df_priorities = DataHandler.get_element_priorities(PRIORITIES_FILE)
	#print(df_priorities)

	# pull in base XBRL SQL query
	SQLQuery = sql_to_text(SQL_FILE)
	dat_tableInfo = []

	# run main query for each entity, replacing entity from list of entities in SQL
	for ii in range(num_entities):

		# stop after hitting the processing limit
		if ii >= NUM_ENTITIES_TO_QUERY:
			break

		# run query against XBRL for entity
		else:
			ent = list_entities[ii]
			print(ent)

			# replace entity in each SQL query
			SQLQueryRepl = SQLQuery.replace('%AMAZON%', ent)
			dat_tableInfo.append(DataHandler.xbrl_query(XBRL, SQLQueryRepl))


	#print(dat_tableInfo)
	#print("length dat_tableInfo:", len(dat_tableInfo))

	# create dummy frame to fill with indiviual entity data frames
	dat_dfInfo = pd.DataFrame(columns=[
		'entity.entity_name',
		'fact.element_local_name',
		'fact.effective_value',
		'fact.fiscal_year'
		])

	# loop through the data retrieved from XBRL in list format, then convert to data frame and append to full set, at entity-entry level
	for entity_entries in dat_tableInfo:
		dat_dfInfo_temp = DataHandler.dat_to_dataframe(entity_entries, [
			##'fact.fact_id',
			'entity.entity_name',
			'fact.element_local_name',
			'fact.effective_value',
			##'fact.uom',
			##'fact.accession_id',
			##'fact.element_id',
			'fact.fiscal_year'
			##'context.fiscal_period',
			##'context.period_start',
			##'context.period_end'
			])

		dat_dfInfo = dat_dfInfo.append(dat_dfInfo_temp)

	#print(dat_tableInfo)

	# convert values to numeric
	dat_dfInfo['fact.effective_value'] = pd.to_numeric(dat_dfInfo['fact.effective_value'])

	# cast/pivot table with years from left-to-right
	dat_castDF = dat_dfInfo.pivot_table(
		values = 'fact.effective_value' ,
		index = ['entity.entity_name', 'fact.element_local_name'] ,
		columns = 'fact.fiscal_year'
		)

	# slice is (start, end) in this context
	#print(list(dat_castDF.columns))
	#print(list(dat_castDF.index.get_level_values('fact.element_local_name').unique()))
	#print(dat_castDF.loc[ ( slice('Apple Inc.', 'Apple Inc.'), slice( 'AssetsCurrent' , 'AssetsCurrent' ) ) , slice( '2016' , '2016' ) ])

	# determine boundaries of data: entities, elements & years
	dat_entities = list(dat_castDF.index.get_level_values('entity.entity_name').unique())
	dat_elements = list(dat_castDF.index.get_level_values('fact.element_local_name').unique())
	dat_years = list(dat_castDF.loc[ ( slice( None ), slice( None ) ) , : ].columns.unique())

	cols = ['entity.entity_name', 'fact.element_local_name', 'year', 'element', 'element_priority', 'value']
	msg = "Parsing F-Score for: {} on {} over {}".format(dat_entities, dat_elements, dat_years)
	print(msg)

	df_fscorePriorities = pd.DataFrame(columns=cols)

	# loop over each entity, element, and year to determine element availability, class, and priority
	for ent in dat_entities:

		# different entities will have different bounds, need to handle empty cases
		for elem in dat_elements:

			# expecting IndexError due to different element, years, entities and differences in filing dates or database updates
			try:
				tempValues = []
				tempDeltaInd = []

				for yr in dat_years:
					elementValue = dat_castDF.loc[ ( slice( ent , ent), slice( elem , elem ) ) , slice( yr , yr ) ].values[0][0]

					# look up element, its class, and its priority
					df_priority_row = df_priorities.loc[ df_priorities['fact.element_local_name'] == elem ]
					print(df_priority_row)
					elemClass = df_priority_row['element'].values[0]
					elemPriority = df_priority_row['priority'].values[0]

					tempRow = [ent] + [elem] + [yr] +  [elemClass] + [elemPriority] + [elementValue]
					tempDict = dict(zip(cols, tempRow))
					#print(tempDict)
					df_fscorePriorities = df_fscorePriorities.append(tempDict, ignore_index=True)

			except IndexError:

				#nodata_row = [ent] + [elem] + ([None] * len(dat_years))  # create a dummy row indicating that the entity-element combination doesn't exist
				nodata_row = [ent] + [elem] + ([None] * 4)
				tempDict = dict(zip(cols, nodata_row))
				df_fscorePriorities = df_fscorePriorities.append(tempDict, ignore_index=True)
				continue

	# convert values to numeric
	df_fscorePriorities['element_priority'] = pd.to_numeric(df_fscorePriorities['element_priority'])

	print(df_fscorePriorities.to_string())

	df_fscorePrioritiesFiltered = pd.DataFrame(columns=cols)


	print(df_fscorePriorities.loc[df_fscorePriorities.reset_index().groupby(['entity.entity_name', 'year', 'element'])['element_priority'].idxmin()].to_string())


	# test query, return table information from entity table
	#sqlQuery = "SELECT * FROM accession limit 10"
	#sqlQuery = "SELECT * FROM INFORMATION_SCHEMA.COLUMNS where TABLE_NAME='accession'"
	#dat_tableInfo = DataHandler.xbrl_query(XBRL, sqlQuery)
	#print(dat_tableInfo)

	# get list of all entities in db
	#sqlQuery = "SELECT DISTINCT entity_name, entity_id, entity_code FROM entity"
	#dat_entityInfo = DataHandler.xbrl_query(XBRL, sqlQuery)
	#df_entities = DataHandler.dat_to_dataframe(dat_entityInfo, ['entity_name', 'entity_id', 'entity_code'])
	#print(dat_entityInfo[0])

	# get list of SIC codes from accession table
	#sqlQuery = "SELECT COUNT(DISTINCT entity_id, standard_industrial_classification) FROM accession"
	#dat_accessionInfo = DataHandler.xbrl_query(XBRL, sqlQuery)
	#df_accession = DataHandler.dat_to_dataframe(dat_accessionInfo, ['entity_id', 'sic_code', 'accession_id', 'filing_date', 'document_type'])
	#df_accession = DataHandler.dat_to_dataframe(dat_accessionInfo, ['count_sic'])

	#print(dat_accessionInfo[0])


# Net Income = ['NetIncomeLoss', 'OperatingIncomeLoss']
# Assets = ['Assets']
# Current Assets = ['AssetsCurrent', 'AssetsCurrentAbstract']
# Liabilities = ['LiabilitiesCurrent', 'LiabilitiesCurrentAbstract']
# Long Term Debt = ['LongTermDebtAbstract','LongTermDebt', 'LiabilitiesNoncurrent', 'LongTermDebtNoncurrent']
# COGS = ['CostOfGoodsSold', 'CostOfGoodsAndServicesSold', 'CostOfGoodsSoldExcludingDepreciationDepletionAndAmortization', 'CostOfGoodsAndServicesSold', 'CostOfGoodsSoldAbstract', 'CostOfRevenue', 'CostOfRevenueAbstract']
# Gross Profit = ['Revenues', 'SalesRevenueGoodsNet', 'SalesRevenueNet', 'GrossProfit']
# Share Issuance = ['CommonStockSharesOutstanding']
# Cash Flow from Operations = ['NetCashProvidedByUsedInOperatingActivitiesAbstract']
